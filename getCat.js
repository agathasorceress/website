const Http = new XMLHttpRequest();
const url='https://api.thecatapi.com/v1/images/search';
Http.open("GET", url);
Http.send();

Http.onreadystatechange = function() {
   if (this.readyState == 4 && this.status == 200) {
     console.log(Http.json)
     var json = JSON.parse(Http.responseText);
     var img = document.createElement("img");
     img.src = json[0].url;
     img.id = "cat";
     img.alt = "A cat picture";
     document.getElementById("content").appendChild(img);
   }
}
